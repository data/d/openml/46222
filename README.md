# OpenML dataset: USGasoline

https://www.openml.org/d/46222

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Weekly U.S. Product Supplied of Finished Motor Gasoline (Thousand Barrels per Day).

Provided by U.S. Energy Information AdministratiQon. Downloaded from the website as available on 24-05-2024.

There are 4 columns:

id_series: The id of the time series.

date: The date of the time series in the format "%Y-%m-%d %H:%M:%S".

time_step: The time step on the time series.

value_0: The values of the time series, which will be used for the forecasting task.

Preprocessing:

1 - Renamed column to 'Week of' to 'date'

2 - Parsed the date with the format "%m/%d/%Y" and converted it to string with format ''%Y-%m-%d %H:%M:%S'.

3 - Renamed column 'Weekly U.S. Product Supplied of Finished Motor Gasoline Thousand Barrels per Day' to 'value_0'.

4 - Created 'id_series' with value 0. There is only one time series.

5 - Ensured that there are no missing dates and that the frequency of the time_series is 7 days.

6 - Created 'time_step' column from 'date' and 'id_series' with increasing values from 0 to the size of the time series.

7 - Casted 'date' to str, 'time_step' to int, 'value_0' to int, and defined 'id_series' as 'category'.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46222) of an [OpenML dataset](https://www.openml.org/d/46222). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46222/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46222/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46222/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

